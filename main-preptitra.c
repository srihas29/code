/* Matthias Ullmann                                     */
/* Bayreuth, November 2017                             */
/*                                                      */
/* .....................................................*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include "properties.h"
#include "mathematik.h"
#include "mol.h"
#include "util.h"
#include "memutil.h"
#include "preptitra.h"
#include "gmct.h"


/* .......................................................*/
void WriteBACKpqr(Sites *sites, Mol *p)
{
  int i, j, h;
  Site *s;
  char name[LINELENGTH];
  FILE *fp;
  double crg;

  for (i=1; i<=(*sites).nsites; i++)
      {s = &(*sites).site[i];
       sprintf(name,"%s/%s-BACK.pqr", DIR, (*sites).name[i]);
       crg = 0.0;
       fp=file_open(name,"w");
       /* write the SITE atoms with zero charge */
       for (j=1; j<=(*s). nsiteatoms; j++)
	   {h=(*s).siteatoms[j];
	    fprint_PQRXatom(fp, p, h);
	    crg = crg + (*p).charge[h];
	   }
       /* write the BACK atoms */
       for (j=1; j<=(*s). nbackatoms; j++)
	   {h=(*s).backatoms[j];
	    fprint_PQRXatom(fp, p, h);
	    crg = crg + (*p).charge[h];
	   }
       fprintf(stdout,"Total Charge of %-50s: %12.5f\n", name, crg);
       CHARGE_ERROR(crg);
       fclose(fp);
      }

}
/* .......................................................*/
void WriteSITEpqr(Sites *sites, Mol *p)
{
  int i, j, k,h;
  Site *s;
  char name[LINELENGTH];
  FILE *fp;
  FILE *fp2;
  double crg;

  sprintf(name,"%s/sites.fpt", DIR);
  fp2 = file_open(name, "w");
  for (i=1; i<=(*sites).nsites; i++)
      {s = &(*sites).site[i];
       for (k=1; k<=(*s).nstates; k++)
	   {sprintf(name,"%s/%s-%s.pqr", DIR, (*sites).name[i], (*s).statelabel[k]);
            fp=file_open(name,"w");
	    crg = 0.0;
            for (j=1; j<=(*s). nsiteatoms; j++)
	        {h=(*s).siteatoms[j];
	         (*p).charge[h]=(*s).sitecharges[j][k];
	         crg = crg + (*p).charge[h];
	         fprint_PQRXatom2(fp, p, h, -1);
	   
	         fprintf(fp2,"%4d %4d  %8.3lf  %8.3lf  %8.3lf  %12.8lf\n",
	   	       i, k, (*p).xyz[h][1], (*p).xyz[h][2], (*p).xyz[h][3], (*p).charge[h]);
	         (*p).charge[h]=0.0;
	        }
	    fprintf(stdout,"Total Charge of %-50s: %12.5f\n", name, crg);
            CHARGE_ERROR(crg);
            fclose(fp);
	   }
      }
  fclose(fp2);

}

/* .......................................................*/
void DetermineCenters_OLD(Sites *sites, Mol *p)
{
  int i, j, h;
  Site *s;

  for (i=1; i<=(*sites).nsites; i++)
      {s = &(*sites).site[i];
       (*s).centerSITE[1]  = 0.0; (*s).centerSITE[2]  = 0.0; (*s).centerSITE[3]  = 0.0;
       (*s).centerMODEL[1] = 0.0; (*s).centerMODEL[2] = 0.0; (*s).centerMODEL[3] = 0.0;

       h=(*s).siteatoms[1];
       (*s).maxMODEL[1] = (*p).xyz[h][1];
       (*s).maxMODEL[2] = (*p).xyz[h][2];
       (*s).maxMODEL[3] = (*p).xyz[h][3];
       (*s).minMODEL[1] = (*p).xyz[h][1];
       (*s).minMODEL[2] = (*p).xyz[h][2];
       (*s).minMODEL[3] = (*p).xyz[h][3];
       
       (*s).maxSITE[1] = (*p).xyz[h][1];
       (*s).maxSITE[2] = (*p).xyz[h][2];
       (*s).maxSITE[3] = (*p).xyz[h][3];
       (*s).minSITE[1] = (*p).xyz[h][1];
       (*s).minSITE[2] = (*p).xyz[h][2];
       (*s).minSITE[3] = (*p).xyz[h][3];

       for (j=1; j<=(*s). nsiteatoms; j++)
	   {h=(*s).siteatoms[j];
            (*s).centerSITE[1]  = (*s).centerSITE[1]  + (*p).xyz[h][1];
            (*s).centerSITE[2]  = (*s).centerSITE[2]  + (*p).xyz[h][2];
            (*s).centerSITE[3]  = (*s).centerSITE[3]  + (*p).xyz[h][3];
	    if ((*s).maxSITE[1]<(*p).xyz[h][1]) {(*s).maxSITE[1]=(*p).xyz[h][1];}
	    if ((*s).maxSITE[2]<(*p).xyz[h][2]) {(*s).maxSITE[2]=(*p).xyz[h][2];}
	    if ((*s).maxSITE[3]<(*p).xyz[h][3]) {(*s).maxSITE[3]=(*p).xyz[h][3];}
	    if ((*s).minSITE[1]>(*p).xyz[h][1]) {(*s).minSITE[1]=(*p).xyz[h][1];}
	    if ((*s).minSITE[2]>(*p).xyz[h][2]) {(*s).minSITE[2]=(*p).xyz[h][2];}
	    if ((*s).minSITE[3]>(*p).xyz[h][3]) {(*s).minSITE[3]=(*p).xyz[h][3];}

            (*s).centerMODEL[1] = (*s).centerMODEL[1] + (*p).xyz[h][1];
            (*s).centerMODEL[2] = (*s).centerMODEL[2] + (*p).xyz[h][2];
            (*s).centerMODEL[3] = (*s).centerMODEL[3] + (*p).xyz[h][3];	    
	    if ((*s).maxMODEL[1]<(*p).xyz[h][1]) {(*s).maxMODEL[1]=(*p).xyz[h][1];}
	    if ((*s).maxMODEL[2]<(*p).xyz[h][2]) {(*s).maxMODEL[2]=(*p).xyz[h][2];}
	    if ((*s).maxMODEL[3]<(*p).xyz[h][3]) {(*s).maxMODEL[3]=(*p).xyz[h][3];}
	    if ((*s).minMODEL[1]>(*p).xyz[h][1]) {(*s).minMODEL[1]=(*p).xyz[h][1];}
	    if ((*s).minMODEL[2]>(*p).xyz[h][2]) {(*s).minMODEL[2]=(*p).xyz[h][2];}
	    if ((*s).minMODEL[3]>(*p).xyz[h][3]) {(*s).minMODEL[3]=(*p).xyz[h][3];}
	   }
       for (j=1; j<=(*s). nbackatoms; j++)
	   {h=(*s).backatoms[j];
            (*s).centerMODEL[1] = (*s).centerMODEL[1] + (*p).xyz[h][1];
            (*s).centerMODEL[2] = (*s).centerMODEL[2] + (*p).xyz[h][2];
            (*s).centerMODEL[3] = (*s).centerMODEL[3] + (*p).xyz[h][3];
	    if ((*s).maxMODEL[1]<(*p).xyz[h][1]) {(*s).maxMODEL[1]=(*p).xyz[h][1];}
	    if ((*s).maxMODEL[2]<(*p).xyz[h][2]) {(*s).maxMODEL[2]=(*p).xyz[h][2];}
	    if ((*s).maxMODEL[3]<(*p).xyz[h][3]) {(*s).maxMODEL[3]=(*p).xyz[h][3];}
	    if ((*s).minMODEL[1]>(*p).xyz[h][1]) {(*s).minMODEL[1]=(*p).xyz[h][1];}
	    if ((*s).minMODEL[2]>(*p).xyz[h][2]) {(*s).minMODEL[2]=(*p).xyz[h][2];}
	    if ((*s).minMODEL[3]>(*p).xyz[h][3]) {(*s).minMODEL[3]=(*p).xyz[h][3];}
	   }
       (*s).centerSITE[1]  = (*s).centerSITE[1] /((*s).nsiteatoms);
       (*s).centerSITE[2]  = (*s).centerSITE[2] /((*s).nsiteatoms);
       (*s).centerSITE[3]  = (*s).centerSITE[3] /((*s).nsiteatoms);
       (*s).centerMODEL[1] = (*s).centerMODEL[1]/((*s).nsiteatoms + (*s).nbackatoms);
       (*s).centerMODEL[2] = (*s).centerMODEL[2]/((*s).nsiteatoms + (*s).nbackatoms);
       (*s).centerMODEL[3] = (*s).centerMODEL[3]/((*s).nsiteatoms + (*s).nbackatoms);
      }
}
/* .......................................................*/
void DetermineCenters(Sites *sites, Mol *p)
{
  int i, j, h;
  Site *s;

  for (i=1; i<=(*sites).nsites; i++)
      {s = &(*sites).site[i];
       h=(*s).siteatoms[1];
       (*s).maxMODEL[1] = (*p).xyz[h][1]; (*s).maxMODEL[2] = (*p).xyz[h][2]; (*s).maxMODEL[3] = (*p).xyz[h][3];
       (*s).minMODEL[1] = (*p).xyz[h][1]; (*s).minMODEL[2] = (*p).xyz[h][2]; (*s).minMODEL[3] = (*p).xyz[h][3];
       
       (*s).maxSITE[1] = (*p).xyz[h][1]; (*s).maxSITE[2] = (*p).xyz[h][2]; (*s).maxSITE[3] = (*p).xyz[h][3];
       (*s).minSITE[1] = (*p).xyz[h][1]; (*s).minSITE[2] = (*p).xyz[h][2]; (*s).minSITE[3] = (*p).xyz[h][3];

       for (j=1; j<=(*s). nsiteatoms; j++)
	   {h=(*s).siteatoms[j];
            if ((*s).maxSITE[1]<(*p).xyz[h][1]) {(*s).maxSITE[1]=(*p).xyz[h][1];}
	    if ((*s).maxSITE[2]<(*p).xyz[h][2]) {(*s).maxSITE[2]=(*p).xyz[h][2];}
	    if ((*s).maxSITE[3]<(*p).xyz[h][3]) {(*s).maxSITE[3]=(*p).xyz[h][3];}
	    if ((*s).minSITE[1]>(*p).xyz[h][1]) {(*s).minSITE[1]=(*p).xyz[h][1];}
	    if ((*s).minSITE[2]>(*p).xyz[h][2]) {(*s).minSITE[2]=(*p).xyz[h][2];}
	    if ((*s).minSITE[3]>(*p).xyz[h][3]) {(*s).minSITE[3]=(*p).xyz[h][3];}

            if ((*s).maxMODEL[1]<(*p).xyz[h][1]) {(*s).maxMODEL[1]=(*p).xyz[h][1];}
	    if ((*s).maxMODEL[2]<(*p).xyz[h][2]) {(*s).maxMODEL[2]=(*p).xyz[h][2];}
	    if ((*s).maxMODEL[3]<(*p).xyz[h][3]) {(*s).maxMODEL[3]=(*p).xyz[h][3];}
	    if ((*s).minMODEL[1]>(*p).xyz[h][1]) {(*s).minMODEL[1]=(*p).xyz[h][1];}
	    if ((*s).minMODEL[2]>(*p).xyz[h][2]) {(*s).minMODEL[2]=(*p).xyz[h][2];}
	    if ((*s).minMODEL[3]>(*p).xyz[h][3]) {(*s).minMODEL[3]=(*p).xyz[h][3];}
	   }
       for (j=1; j<=(*s). nbackatoms; j++)
	   {h=(*s).backatoms[j];
            if ((*s).maxMODEL[1]<(*p).xyz[h][1]) {(*s).maxMODEL[1]=(*p).xyz[h][1];}
	    if ((*s).maxMODEL[2]<(*p).xyz[h][2]) {(*s).maxMODEL[2]=(*p).xyz[h][2];}
	    if ((*s).maxMODEL[3]<(*p).xyz[h][3]) {(*s).maxMODEL[3]=(*p).xyz[h][3];}
	    if ((*s).minMODEL[1]>(*p).xyz[h][1]) {(*s).minMODEL[1]=(*p).xyz[h][1];}
	    if ((*s).minMODEL[2]>(*p).xyz[h][2]) {(*s).minMODEL[2]=(*p).xyz[h][2];}
	    if ((*s).minMODEL[3]>(*p).xyz[h][3]) {(*s).minMODEL[3]=(*p).xyz[h][3];}
	   }
       (*s).centerSITE[1]  = 0.5*((*s).maxSITE[1]+(*s).minSITE[1]);
       (*s).centerSITE[2]  = 0.5*((*s).maxSITE[2]+(*s).minSITE[2]);
       (*s).centerSITE[3]  = 0.5*((*s).maxSITE[3]+(*s).minSITE[3]);

       (*s).centerMODEL[1] = 0.5*((*s).maxMODEL[1]+(*s).minMODEL[1]);
       (*s).centerMODEL[2] = 0.5*((*s).maxMODEL[2]+(*s).minMODEL[2]);
       (*s).centerMODEL[3] = 0.5*((*s).maxMODEL[3]+(*s).minMODEL[3]);
      }
}
/* ............................................................. */
/* write a shell script for doing the electrostatics calculation */
/* Since some part of it can be parallelized using xjobs,        */
/* the script is written such that is writes a bourne shell      */
/* that is started within the script                             */
/*                                                               */
void WriteShellScript(PrepTitraPara *para, Sites *sites)
{
  FILE *fp;
  char name[LINELENGTH];
  Site *s;
  int i, k;

  /* first write the main script that is also calling the 
     next script using */
  sprintf(name,"%s/job.csh", DIR);
  fp = file_open(name, "w");
  fprintf(fp,"#!/bin/csh -xv\n");
  fprintf(fp,"set temp     = %e\n",(*para).temp);
  fprintf(fp,"set solrad   = %e\n",(*para).solrad);
  fprintf(fp,"set stern    = %e\n",(*para).stern);
  fprintf(fp,"set epssol   = %e\n",(*para).epssol);
  fprintf(fp,"set ionicstr = %e\n",(*para).ionicstr);
  fprintf(fp,"set epsin    = %e\n",(*para).epsin);
  fprintf(fp,"set bin      = %s\n",(*para).meaddir);
  fprintf(fp,"set flag     = \"-blab3 -T $temp -epsext $epssol -sterln $stern -solrad $solrad -ionicstr $ionicstr\" \n");

  fprintf(fp,"#===========================================================\n");
  fprintf(fp,"$bin/pqr2SolvAccVol -solrad $solrad %s-PROTEIN\n", (*para).name);
  fprintf(fp,"$bin/pqr2IonAccVol  -sterln $stern  %s-PROTEIN\n", (*para).name);
  fprintf(fp,"#===========================================================\n");

  fprintf(fp,"#===========================================================\n");
  fprintf(fp,"cat << EOF >! job-all.sh\n");
  fprintf(fp,"#!/bin/sh -xv\n");
  for (i=1; i<=(*sites).nsites; i++)
      {s = &(*sites).site[i];
       for (k=1; k<=(*s).nstates; k++)
	   {sprintf(name,"%s-%s",(*sites).name[i], (*s).statelabel[k]);
	    fprintf(fp,"$bin/my_2diel_solver $flag -epsin  $epsin %s %s-BACK > %s-MODEL.out\n",
		    name, (*sites).name[i], name);
	    fprintf(fp,"$bin/my_3diel_solver $flag -epsin1 $epsin -epsin2 $epsin -eps2set %s-PROTEIN  -fpt sites %s %s-PROTEIN > %s-PROTEIN.out\n",
		    (*para).name, name, (*para).name, name);
	   }
       fprintf(fp,"#===========================================================\n");
      }
  fprintf(fp,"EOF\n");
  fprintf(fp,"chmod +x job-all.sh\n");
  // Change -- 'nproc' should be a parameter
  fprintf(fp,"xjobs -j%d -s job-all.sh\n",(*para).nproc);
  fclose(fp);

}
/* .......................................................*/
void CheckGrids(PrepTitraPara *para, Sites *sites, Mol *p)
{
  double max[4], min[4];
  int i;
  Site *s;


  /* check if the outermost grid is large enough to hold
     the whole protein, namely  there should be also a safety space */
  GridExtension(max, min, (*para).center, (*para).spacing[1], (*para).gridpoints[1]);
  fprintf(stdout,"Checking the outermost grid.\n");
  CheckGridSize(max, min, (*p).max, (*p).max, (*para).ext_safety, (*para).spacing[1]);
  
  /* check if the grid spacing is decreasing in each focusing step */
  CheckFocussingSteps((*para).spacing, (*para).nfocus);

  /* now check if the boxes fit into each other */
  fprintf(stdout,"Checking the Grid Foccusing\n");
  CheckFocussingGrids(sites, max, min, (*para).spacing,
		      (*para).gridpoints, (*para).nfocus);
  
  /* now check if the innermost grids are large enough */
  for (i=1; i<=(*sites).nsites; i++)
      {s = &(*sites).site[i];
       GridExtension(max, min, (*s).centerSITE, (*para).spacing[(*para).nfocus],
		     (*para).gridpoints[(*para).nfocus]);
       fprintf(stdout,"Checking the innermost grid for %-30s: \t", (*sites).name[i]);
       fprintf(stdout,"         Spacing: %8.3f GridPoints: %4d\n",
	       (*para).spacing[(*para).nfocus], (*para).gridpoints[(*para).nfocus]);
       CheckGridSize(max, min, (*s).maxSITE, (*s).minSITE, (*para).int_safety, (*para).spacing[(*para).nfocus]);
      }
 
}
/* .......................................................*/
void WriteGrids(PrepTitraPara *para, Sites *sites)
{
  FILE *fp;
  int i, j, k;
  char name[LINELENGTH];
  Site *s;
  

  for (i=1; i<=(*sites).nsites; i++)
      {s = &(*sites).site[i];
       for (k=1; k<=(*s).nstates; k++)
	   {sprintf(name,"%s/%s-%s.ogm", DIR, (*sites).name[i], (*s).statelabel[k]);
            fp=file_open(name,"w");
	    fprintf(fp,"(%8.3f, %8.3f, %8.3f)  %4d  %8.3f\n",
		    (*para).center[1],(*para).center[2],(*para).center[3],
		    (*para).gridpoints[1], (*para).spacing[1]);
	    for (j=2; j<=(*para).nfocus; j++)
	        {fprintf(fp,"(%8.3f, %8.3f, %8.3f)  %4d  %8.3f\n",
			 (*s).centerSITE[1],(*s).centerSITE[2],(*s).centerSITE[3],
		         (*para).gridpoints[j], (*para).spacing[j]);
		}
	    fclose(fp);
	   }
      }
}

/* .................................................... */

void WriteProteinBackgroundPQR(PrepTitraPara *para, Mol *p)
{
  char name[LINELENGTH];
  FILE *fp;
  double crg;

  /* Check if charge is an integer */
  crg = charge((*p).charge, (*p).natoms);
  fprintf(stdout,"Total Charge of the Protein Background: %12.5f\n",crg);
  CHARGE_ERROR(crg);
  /* CHANGE -- PQR <==> PQRX??? */
  recount_aa_with_chain1000(p);/* CHANGE */
  sprintf(name,"mkdir %s", DIR);
  if (0!=system(name)) /* make the directory */
     {fprintf(stderr,"%s\n",name);
      muwarn("Problem while making a directory, maybe already existing");
     }
  sprintf(name,"%s/%s-PROTEIN.pqr", DIR, (*para).name);
  fp=file_open(name,"w");
  fprint_PQRX(fp, p);
  fclose(fp);

}
/* .................................................... */
void CheckGeometry(Sites *sites, Mol *p)
{
  Site *s;
  int i, j;
  double dist, sum, vol, dihe;

  /* ++++++++++++++++++++++++++++++++++++++++++ */
  /* Check if the atoms are in bonding distance */
  for (i=1; i<=(*sites).nsites; i++)
      {s = &(*sites).site[i];
       for (j=1; j<=(*s).nbond; j++)
	   {dist = Distance((*p).xyz[(*s).bondatoms[j][1]],
			    (*p).xyz[(*s).bondatoms[j][2]]);
	    sum = (*p).covrad[(*s).bondatoms[j][1]]+(*p).covrad[(*s).bondatoms[j][2]];
	    if (dist > sum)
	       {fprintf(stderr, "The following two atoms are not in bonding distance:\n");
		fprintf(stderr, "%s --- %s  -->  Distance: %8.3f --- Covalent Distance: %8.3f\n",
		        (*s).bond1[j], (*s).bond2[j], dist, sum);
		fprintf(stderr, "Probably problem in XST-file BOND record or in the residue assignment (sites-file)\n");
	        muerror("Detected in CheckGeometry() 1");
	       }
	   }
      }
  /* ++++++++++++++++++++++++++++++++++++++++++ */
  /* Check bonding chirality                    */
  for (i=1; i<=(*sites).nsites; i++)
      {s = &(*sites).site[i];
       for (j=1; j<=(*s).nchiral; j++)
	   {vol = ChiralVolume((*p).xyz[(*s).chiralatoms[j][1]], (*p).xyz[(*s).chiralatoms[j][2]],
			       (*p).xyz[(*s).chiralatoms[j][3]], (*p).xyz[(*s).chiralatoms[j][4]]);
	    if (   ((vol <= 0) && ((*s).chiralVol[j] == +1))
		|| ((vol >= 0) && ((*s).chiralVol[j] == -1)))
	       {fprintf(stderr, "The following four atoms have the wrong sign of the Chiral Volume:\n");
		fprintf(stderr, "%s -- %s -- %s -- %s \n",
			(*s).chiral1[j], (*s).chiral2[j], (*s).chiral3[j], (*s).chiral4[j]);
		fprintf(stderr, "Chiral Volume: %8.3f --- Expected 'Sign': %d\n",
			vol, (*s).chiralVol[j]);
		fprintf(stderr, "Probably problem in XST-file CHIRAL record or in the residue assignment (SITES-file)\n");
		fprintf(stderr, "or problem in the naming convention in the PQR-file\n");
		muerror("Detected in CheckGeometry() 2");
	       }
	   }
      }
  
  /* ++++++++++++++++++++++++++++++++++++++++++ */
  /* Check bonding dihedral                     */
  for (i=1; i<=(*sites).nsites; i++)
      {s = &(*sites).site[i];
       for (j=1; j<=(*s).ndihe; j++)
	   {dihe = Dihedral((*p).xyz[(*s).diheatoms[j][1]], (*p).xyz[(*s).diheatoms[j][2]],
			    (*p).xyz[(*s).diheatoms[j][3]], (*p).xyz[(*s).diheatoms[j][4]],
			    (*p).xyz[(*s).diheatoms[j][5]], (*p).xyz[(*s).diheatoms[j][6]]);
	     if (fabs(dihe)>= (1.5*M_PI))
	        {muwarn ("fabs(dihe)>= (1.5*M_PI) -- Potential problem in CheckGeometry() 4 ???");
		 if (dihe>0.0) dihe = dihe - 2.0*M_PI;
		 if (dihe<0.0) dihe = dihe + 2.0*M_PI;
		}
	     if (   ((fabs(dihe) <= (0.5*M_PI)) && ((*s).orientation[j] == 1))
		||  ((fabs(dihe) >= (0.5*M_PI)) && ((*s).orientation[j] == 0)))
	       {fprintf(stderr, "The following six atoms have the wrong orientation:\n");
		fprintf(stderr, "%s -- %s -- %s -- %s -- %s -- %s \n",
			(*s).dihe1[j], (*s).dihe2[j], (*s).dihe3[j], (*s).dihe4[j], (*s).dihe5[j], (*s).dihe6[j]);
		fprintf(stderr, "Dihedral: %8.3f --- Expected Orientation: %d\n",
			TOGRD(dihe), (*s).orientation[j]);
		fprintf(stderr, "Probably problem in XST-file DIHE record or in the residue assignment (SITES-file)\n");
		fprintf(stderr, "or problem in the naming convention in the PQR-file\n");
		muerror("Detected in CheckGeometry() 3");
	       }
	   }
      }
}

/*************************************************************************************/

void WriteAPBSinput(PrepTitraPara *para, Sites *sites)

{
  FILE *fp;
  char name[LINELENGTH];
  Site *s;
  int i, k,fsteps,maxsteps = (*para).nfocus, dime[10];
  int *pdime;
  pdime = &dime[0];
  *pdime = 161;
  *(pdime+1) = 161;
  *(pdime+2) = 161;
  *(pdime+3) = 65;
  *(pdime+4) = 65;
  *(pdime+5) = 65;
  
  double *pgrid, grid[10];
  pgrid = &grid[0];

  *pgrid = 4.0;
  *(pgrid+1) = 2.0 ;
  *(pgrid+2) = 1.0 ;
  *(pgrid+3) = 0.4 ;
  *(pgrid+4) = 0.2 ;
  *(pgrid+5) = 0.1 ;

  /* Solving for grid dimensions around the center     
	case: nlev = 4
  

  int c[5],*pc, dime_arr[5], *pdime_arr ;
  pc = &c[0];
  pdime_arr = &dime_arr[0];
  *(c) = 1, *(c+1) = 2, *(c+2) = 3,*(c+3) = 4,*(c+4) = 5;
  *(pdime_arr) = 33,*(pdime_arr+1) = 65,*(pdime_arr+2) = 97,*(pdime_arr+3) = 129,*(pdime_arr+4) = 161;

  double gridlen[20], *pgridlen;

  for (fsteps=1; fsteps<=maxsteps;fsteps++)
  {
  
  
  
  
  
  }

 */

  /* Determine number of focussing steps.
  Set parameters for focusing loop
  
  */
  char letters[27] = "abcdefghijklmnopqrstuvwxyz";

/* Other APBS parameters */

int mol = 1,plus = 1, minus = -1;
double pdie = 1.0, sdie =78.54, srad = 1.4, swin = 0.3, sdens = 10.0, temp = 298.15;
double iconc = 0.1, irad = 2.0;

  for (i=1; i<=(*sites).nsites; i++)
      {s = &(*sites).site[i];
       for (k=1; k<=(*s).nstates; k++)
	   {sprintf(name,"%s/%s-%s_APBS.in",DIR,(*sites).name[i], (*s).statelabel[k]);
      fp = file_open(name, "w");

      for (fsteps=1; fsteps<=maxsteps;fsteps++)
      {
      if (fsteps==1)
      {fprintf(fp,"read #molecule %s\n", (*para).name);
      fprintf(fp,"  mol pqr %s-%s_APBS.pqr\n",(*sites).name[i], (*s).statelabel[k]);
      fprintf(fp,"end\n\n");
      }
      fprintf(fp,"elec name %c \n",letters[fsteps-1]);
      fprintf(fp,"  mg-manual\n");
      fprintf(fp,"  dime %d %d %d\n",*(pdime+fsteps-1), *(pdime+fsteps-1), *(pdime+fsteps-1));
      fprintf(fp,"  nlev 4\n");
      fprintf(fp,"  grid %lf %lf %lf\n", *(pgrid+fsteps-1), *(pgrid+fsteps-1), *(pgrid+fsteps-1));

      if (fsteps==1)
      {
      fprintf(fp,"  gcent %lf %lf %lf\n",(*para).center[1], (*para).center[2], (*para).center[3]);
      }

      else
      {
      fprintf(fp,"  gcent %lf %lf %lf\n",(*s).centerSITE[1], (*s).centerSITE[2], (*s).centerSITE[3]);
      }
      
      fprintf(fp,"  mol %d\n",mol);
      fprintf(fp,"  lpbe\n");
      if (fsteps==1)
      {
      fprintf(fp,"  bcfl mdh\n");
      }
      else
      {
      fprintf(fp,"  bcfl focus\n");
      }
      fprintf(fp,"  pdie %.1lf\n",pdie);
      fprintf(fp,"  sdie %.2lf\n", sdie);
      fprintf(fp,"  ion charge  %d conc %.1lf radius %.1lf\n", plus,iconc,irad);
      fprintf(fp,"  ion charge  %d conc %.1lf radius %.1lf\n", minus, iconc, irad);
      fprintf(fp,"  chgm spl2\n");
      fprintf(fp,"  srfm mol\n");
      fprintf(fp,"  srad %.1lf\n", srad);
      fprintf(fp,"  swin %.1lf\n", swin);
      fprintf(fp,"  sdens %.1lf\n", sdens);
      fprintf(fp,"  temp %.2lf\n", temp);
      fprintf(fp,"  calcenergy total\n");
      fprintf(fp,"  calcforce no\n");
      fprintf(fp,"  write atompot flat AO_%s-%s_%c\n",(*sites).name[i], (*s).statelabel[k],fsteps+'0');
      fprintf(fp,"end\n\n");
      }
      fclose(fp);
	   }
  }
}
//==============================================================================//

void WriteProteinAPBS(Sites *sites, Mol *p)
{
  int i,j, k,l;
  Site *s;
  char name[LINELENGTH];
  FILE *fp;
  double crg;

  for (i=1; i<=(*sites).nsites; i++)
      {s = &(*sites).site[i];
       for (k=1; k<=(*s).nstates; k++)
	   {sprintf(name,"%s/%s-%s_APBS.pqr", DIR, (*sites).name[i], (*s).statelabel[k]);
            fp=file_open(name,"w");
	    crg = 0.0;
      for (l=1;l<=(*p).natoms;l++)
      {
        for (j=1; j<=(*s). nsiteatoms; j++)
        {
          if (l == (*s).siteatoms[j])
            {
	            (*p).charge[l]=(*s).sitecharges[j][k];
              break;
            }
          else 

          {
            (*p).charge[l] = 0.0;
          }
        }

        fprint_PQRChain_atom(fp, p,l);
      }
	    fprintf(stdout,"Total Charge of %-50s: %12.5f\n", name, crg);
            CHARGE_ERROR(crg);
            fclose(fp);
	   }
      }
}
/**********************************************************/
void PrintCharges(Mol *p)

{

int i;

FILE *fp;
fp=file_open("charges.txt", "w");

fprintf(fp,"%s",(*p));
for (i=1;i<=(*p).natoms;i++)
  {
    fprintf(fp,"%lf\n",(*p).charge[i] );
    
    
  }

}

/**********************************************************/
int main(int argc, char *argv[])
 {
  char name[LINELENGTH];
  Mol p;
  PrepTitraPara para;
  Sites sites;
  char type[6];
  
  setbuf(stdout, NULL); /* make sure that everything is written directly */
  usage("usage: preptitra [name]",2,argc);
  version(stdout, "PrepTitra , Part of GMCT", VERSION, "G. Matthias Ullmann");

  strcpy(para.name, argv[1]);
  
  /* read setup and pqr file */
  sprintf(name,"%s.setup", para.name);
  ReadSetupElec(&para);
  strcpy(type,"PQR");
  sprintf(name,"%s.pqr", para.name);
  read_Molecule(name, &p,type);
  AssignIDString(&p);

  sprintf(name,"%s.sites", para.name);
  read_SITES(name, &sites, &p, &para);

  CheckGeometry(&sites, &p);

  /* CHANGE check charges!!! */
  PrintCharges(&p);  /* New command */
  /* assign background charges */
  AssignBackground(&sites, &p, para.blab);
  
  /* determine  center for ogm-files */
  DetermineCenters(&sites, &p);

  /* write background pqr */
  fprintf(stdout,SEPARATE),
  WriteProteinBackgroundPQR(&para, &p);

  /* write all SITE-pqr-files and  sites.fpt for interactions */
  fprintf(stdout,SEPARATE),
  WriteSITEpqr(&sites, &p);

  /* write all BACK pqr */
  fprintf(stdout,SEPARATE),
  WriteBACKpqr(&sites, &p);
  WriteProteinAPBS(&sites, &p); /* New command */ 

  /* get the center, min, and max of the protein */
  properties(&p);
  
  /* write all ogm */
  para.center[1] = 0.5*(p.max[1]+p.min[1]);
  para.center[2] = 0.5*(p.max[2]+p.min[2]);
  para.center[3] = 0.5*(p.max[3]+p.min[3]);
  CheckGrids(&para, &sites, &p);
  WriteGrids(&para, &sites);

  /* write shell script */
  WriteShellScript(&para, &sites);
  WriteAPBSinput(&para, &sites); /* New command */ 
  /* sort ligands??? */
  return 0;
}
