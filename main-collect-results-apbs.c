/* Matthias Ullmann                                     */
/* Bayreuth, November 2017                             */
/*                                                      */
/* .....................................................*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include "properties.h"
#include "mathematik.h"
#include "mol.h"
#include "util.h"
#include "memutil.h"
#include "preptitra.h"
#include "gmct.h"
/***********************************************************/
void GetPotential(char* filename,Mol *p, double arr[]) //double arr[]

{
int natoms;
natoms = (*p).natoms;
FILE *file,*filew;
file = fopen(filename,"r");
//filew = fopen(writefile,"w");

char data[50][1000] ;
double datab[1000]; //static

char buffer[100]; /*Skipping the header lines*/
int i =0;
for (i=0;i<=3;i++)
  {
  fgets(buffer,100,file);
  }


int line=0;
while ((fgets(data[line],50,file) != NULL))
{
	line++;

}


for (i=0;i<=line;i++)

{
	datab[i] = atof(data[i]);
  arr[i] = datab[i];
}

        /*for (i=0;i<=line;i++) Not Needed to write the file
        {

          fprintf(filew,"%lf\n",datab[i]);
        }*/
		
}  



void CalcEnergy(Sites *sites, Mol *p,PrepTitraPara *para)

{
  int i, kcharges ;
  Site *s;
  FILE *fp;
  fp=file_open("apbs.txt", "w");

  s = &(*sites).site[1];
  for (i=1;i<=(*s).nbackatoms;i++)

  {
    kcharges = ((*s).backatoms[i]) ;
    fprintf(fp,"%d \n", kcharges);
  }
  
  

}



void ReadPotentials()


{
  FILE *filep, *fp1;

  filep = fopen("AO_A:2:ASP-ASP.xst-ASPP_6.txt.txt", "r");
  fp1 = fopen("test.txt", "w");

  char buffer[100]; 
  int i =0;
  for (i=0;i<=3;i++)
  {
  fgets(buffer,100,filep);
  }

  char data[100][1000];
  int line =0;
  while (fgets(data[line],1000,filep) != NULL)

  {
    line++;
    
  }

  

  double datam[100];
  
  for (i=0;i<=56;i++){

  datam[i] = atof(data[i]);
  fprintf(fp1,"%lf\n",datam[i]);
  }


  fclose(filep);

}

void writefile(double arr[],char* myfile,int n)
{
  FILE *file;
  file = fopen(myfile,"w");
  int i=0;
  for (i=0;i<=n;i++)
  {
    fprintf(file,"%lf\n",arr[i]);
  }
} 

void IteratePot(Sites *sites,Mol *p)

{
  #define stepsnfocus 6
  #define totalatoms 56
  bool cond1, cond2, cond3;
  Site *s, *sitep;
  Mol back;
  char backname[100];
  char potname[100];
  char potoutput[100];
  char potmodel[100];
  char potbackmodel[100];
  //double potofmodel[100] = {0.00};
  // Replace with [*para.nfocus][*p.natoms]
  double arr[stepsnfocus ][totalatoms];
  double potmerge[totalatoms]; 
  double potofmodel[100]; //Number of site atoms in each state
  double potofback[100];
  int atnum = 0;
  //double potvalue[2][2] = 0.0;
  double pvalue =0.0;

  FILE *fileb, *fileback, *fileinter;
  fileb = fopen("Born","w");
  fileback = fopen("Back","w");
  fileinter = fopen("Inter","w");
  double totalenergy=0.0,totalbackenergy = 0.0,bmodelenergy =0.0;
  
  int i=0,j=0,k=0,l=0,m=0,line=0,numsatms = 0,dummy=0;
  int num_sites = 0, num_states =0, num_siteatoms =0, siteloop = 0;
  double systemenergy =0.0 ;
  int atomsofsite[2][6] = {0};
  int atomsofsite1D[100];

  for (i=1; i<=(*sites).nsites; i++)
  {
    s = &(*sites).site[i]; /*Using this site*/
    ////////////////BACK ENERGY MOLECULE READ IN///////////////////////////////////
    
    sprintf(backname,"%s-BACK.pqr",(*sites).name[i]);
    read_Molecule(backname, &back,"PQR");
    AssignIDString(&back);
    //printf("atom number %d",(back).atomno[1]);
    sprintf(potbackmodel,"AO_%s-BACK.txt",(*sites).name[i]);
    //printf("%s\n",potmodel);
    GetPotential(potbackmodel,&p,potofback);    
    
    ////// MODEL ENERGY TERM ///////

    bmodelenergy = 0;
    for (m=(*s).nsiteatoms+1;m<=back.natoms;m++) // Indexing of siteatoms starts from 1 and use <=
    { 
      
      bmodelenergy += potofback[m-1] * back.charge[m];
      //printf("potential %lf charge %lf\n",potofback[m-1],back.charge[m]);
          
    }
    printf(" MODEL Energy %lf \n",bmodelenergy);
            
    //////////////////////////////////////////////////////////////


    for (k=1; k<=(*s).nstates; k++)
    {
      double arr[stepsnfocus ][totalatoms]  = { 0 }; 
      for (j=0;j<stepsnfocus ;j++) 
      {
      sprintf(potname,"AO_%s-%s_%d.txt",(*sites).name[i], (*s).statelabel[k],j+1);
      //printf("%s\n",potname);
      //printf("Site(i)=%d State(k)=%d Potentialstep (j)=%d\n",i,k,j+1);
      
      GetPotential(potname,&p,arr[j]);
      }
      
      // MODEL POTENTIAL FOR BORN ENERGY
      sprintf(potmodel,"AO_%s-%s_MODEL.txt",(*sites).name[i], (*s).statelabel[k]);
      //printf("%s\n",potmodel);
      GetPotential(potmodel,&p,potofmodel);
      
      double potmerge[totalatoms] = {0.00};
      for (l=0;l<totalatoms;l++)  
        {
          double value = 0.0;
          for (j=(stepsnfocus -1);j>=0;--j) 
          {
            if (arr[j][l] != 0.00)
            {
             value = arr[j][l];
             if (j!=0)
             {
              if ((fabs(arr[j][l] - arr[j-1][l]) > 10000.00) )
              {
                value = arr[j-1][l];
                printf("*********PROBABLY THIS ATOM IS OUT OF THE BOX******************\n");
                printf("Atom=%d Site=%d State=%d\n",l,i,k);
                printf("At %d focussing step the potential is %lf\n", j+1,arr[j][l]);
                printf("And it has been corrected to %lf from the %d focussing step\n",arr[j-1][l],j);
                //printf("old %lf new %lf\n",arr[j][l] ,arr[j-1][l]);
              }

             }
             
             break;
            }
          }
          potmerge[l] = value;
        }

        sprintf(potoutput,"%s-%s-pot.txt",(*sites).name[i],(*s).statelabel[k]);
        writefile(potmerge,potoutput,55);

        
        //////// INTERACTION ENERGY //////////////////
        double ienergy = 0.0,energyistate = 0.0, energyisite = 0.0;
        for (num_sites=1; num_sites<=(*sites).nsites; num_sites++)
        { 
          if (num_sites == i )
          {
            continue;
          }
          sitep = &(*sites).site[num_sites];
          
          energyistate = 0.0;
          for (num_states=1; num_states<=(*sitep).nstates; num_states++)
          {
            ienergy = 0.0;
            for (siteloop=1;siteloop<=(*sitep).nsiteatoms;siteloop++) // Indexing of siteatoms starts from 1 and use <=
            { 
              
              line = (*sitep).siteatoms[siteloop];
              //printf("%lf\n", (*s).sitecharges[m][k]);
              ienergy += (potmerge[line] - potofmodel[siteloop-1]) * (*sitep).sitecharges[siteloop][num_states];
              
              //printf("Site = %d State = %d iSite =%d iState =%d atominstate(inside)=%d atomnuminpqr = %d  ",i,k,num_sites,num_states,siteloop,line);
              //printf("Potential = %lf\n",potmerge[line]);           
            }
            fprintf(fileinter,"Interaction Energy =  %lf Site %d State %d with iSite %d iState %d \n",ienergy,i,k,num_sites,num_states);
            printf("Interaction Energy = %lf Site %s State %s\n",ienergy,(*sites).name[i],(*s).statelabel[k]);
            printf("With iSite = %s iState %s\n",ienergy,(*sites).name[num_sites],(*sitep).statelabel[num_states]);
            
            energyistate += ienergy;

          }

          
          energyisite += energyistate;
          printf("SITE ENERGY = %lf\n",energyisite);
        }

        
        ////////////////// END of interaction energy//////////////////////////////

        double energystate = 0.0;
        line = 0;
        numsatms = (*s).nsiteatoms ;
        for (m=1;m<=numsatms;m++) // Indexing of siteatoms starts from 1 and use <=
        { 
          line = (*s).siteatoms[m];
          //printf("%lf\n", (*s).sitecharges[m][k]);
          energystate += (potmerge[line] - potofmodel[m-1]) * (*s).sitecharges[m][k];
          
        }

        //printf("%lf Site %d State %s \n",energystate,i,(*s).statelabel[k]);
        totalenergy += energystate;
        fprintf(fileb,"Born Energy =  %lf Site %s State %s \n",energystate,(*sites).name[i],(*s).statelabel[k]);

        
        
        
        
        ////////////// Start of Back energy calculation /////////////////////////////
        double modelenergyback = 0.0;
        for (m=1;m<=(*s).nbackatoms;m++)
        {
          line = (*s).backatoms[m];
          modelenergyback += (potofback[m+numsatms-1]) * (*s).backcharges[m]; 
          
        }
        
        //printf("%d\n",(*p).atomno[1]);
        double modelenergyprot = 0.0;
        for (l=1;l<=(*p).natoms;l++)
        {
          int numatom;
          double crgatom;
          numatom = (*p).atomno[l];
          crgatom = (*p).charge[l];
        
          for (num_sites=1; num_sites<=(*sites).nsites; num_sites++)
          {
      
            sitep = &(*sites).site[num_sites];
            for (siteloop=1;siteloop<=(*sitep).nsiteatoms;siteloop++) // To have a clear picture of site atoms
            { 
    
              //printf(" %d\n",(*sitep).siteatoms[siteloop]);
              
              //if (atomsofsite[num_sites][siteloop] == numatom)
              //printf("%d\n",atomsofsite[num_sites][siteloop]);
              
              if (numatom == (*sitep).siteatoms[siteloop])
              {
               crgatom = 0.0;
               break;
              }
              
            }
          

          }

        ///printf("numatom %d charge %lf potential %lf\n",numatom,crgatom,potmerge[l]);
        modelenergyprot += crgatom * potmerge[l];
        

        }
        
        totalbackenergy += (modelenergyprot - bmodelenergy);
        fprintf(fileback,"Back Energy Protein =  %lf Back Energy Model= %lf Site %s State %s \n",modelenergyprot,bmodelenergy,(*sites).name[i],(*s).statelabel[k]);


       //////////////////////back energy end ////////////////////////// 

    } //END OF STATE LOOP

  } // END OF SITE LOOP

fprintf(fileb,"Total Born Energy in multiples of kT/e = %lf\n",totalenergy);
fprintf(fileback,"Total Back Energy in multiples of kT/e = %lf\n",totalbackenergy);
}



/*****/
/**********************************************************/
int main(int argc, char *argv[])
 {
  char name[LINELENGTH];
  Mol p;
  PrepTitraPara para;
  Sites sites;
  char type[6];
  
  setbuf(stdout, NULL); /* make sure that everything is written directly */
  usage("usage: collect-apbs [name]",2,argc);
  version(stdout, "collect-apbs , Part of GMCT", VERSION, "G. Matthias Ullmann");

  strcpy(para.name, argv[1]);
  
  /* read setup and pqr file */
  sprintf(name,"%s.setup", para.name);
  ReadSetupElec(&para);
  strcpy(type,"PQR");
  sprintf(name,"%s.pqr", para.name);
  read_Molecule(name, &p,type);
  AssignIDString(&p);

  sprintf(name,"%s.sites", para.name);
  read_SITES(name, &sites, &p, &para);

 // CalcEnergy(&sites);
 // ReadPotentials() ;

  IteratePot(&sites,&p);

  return 0;
}



















      //double energystate[100] ={0.00};
      
      ////for (m=0;m<;m++)
      //{
        //energystate[k] += (*s).sitecharges[m][k] * ((potmerge[(*s).siteatoms[m]]) - potofmodel[m]);
     //   printf("%d",m);
    //}
      //printf("%lf\n",energystate[k]);
      

      //printf("potmerge=%lf\n",potmerge[9]);
      //sprintf(potoutput,"%s-%s-pot.txt",(*sites).name[i],(*s).statelabel[k]);
      //writefile(potmerge,potoutput,55);
      /*char writepot[100];
      FILE *fstate;
      sprintf(writepot,"potmerge-%s-%s.txt",(*sites).name[i], (*s).statelabel[k]);
      fstate = fopen(writepot,"w");
      for (i=0;i<=55;i++)
      {
        fprintf(fstate,"%lf\n",potmerge[i]);
      }
      fclose(fstate);*/









 /* int atnum = 0;
  //double potvalue[2][2] = 0.0;
  double pvalue =0.0;
   
  for (i=1; i<=(*sites).nsites; i++)
      {
        s = &(*sites).site[i];
        for (k=1; k<=(*s).nstates; k++)
        {

          for (j=1; j<=(*s). nsiteatoms; j++)
          {
            atnum = (*s).siteatoms[j];
            pvalue = (potmerge[atnum])*((*s).sitecharges[j][k]);
            printf("Site %d State %d atnum %d sitecharge %lf potmerge %lf Energy %lf\n",i,k,atnum,(*s).sitecharges[j][k],potmerge[atnum],pvalue);
          }
         
         }      
      }
  
 */




 /*///Site number
  for (j=1; j<=(*s). nsiteatoms; j++)
  {
    atnum = (*s).siteatoms[j];
    (*p).charge[atnum]                 //Charge from pqr file of the siteatom



  }*/
  
  
  //printf("%lf %lf %lf %lf %lf",arr[0][0],arr[1][0],arr[2][0],arr[3][0],arr[4][0],arr[5][0]);
  
  //GetPotential("AO_A:2:ASP-ASP.xst-ASPP_2.txt",&p,arr[1]);
  //printf("%lf %lf",arr[0][0],arr[1][0]);

  //GetPotential("AO_A:2:ASP-ASP.xst-ASPP_1.txt",&p,"pot2.txt");






/**
  FILE *filep, *fp1 ;

  filep = fopen("AO_A:2:ASP-ASP.xst-ASPP_6.txt.txt", "r");
  fp1 = fopen("test.txt", "w");
  

  int line = 0;
  while (!feof(filep) )
    if (fgets(data[line], 1000, filep) != NULL)
     {
      line++;


     }

  fclose(filep);
  
  fprintf(fp1,"nice %s",data[4]);




  
  for (int line =0 ; line < 100 ; line++)
    {
      fscanf(filep,"%lf", &data[line]);
    }
  
  
  fclose(filep);
  
  for (int line =0 ; line < 100 ; line++)
  { 
  fprintf(fp1,"%d %lf\n",line,data[line]);
  }
*/


/****
double* MergePotential(PrepTitraPara *para, Mol *p)

{
  Site *s;
  FILE *file;
  int steps = (*para).nfocus ;
  int atomnum = (*p).natoms ;
  
  static double* potential;
  potential = malloc(sizeof(double[steps][atomnum]));
  //static double potential[6][100] ; // Use dynamic memory allocation later 

  potential = GetPotential("AO_A:2:ASP-ASP.xst-ASPP_6.txt.txt",p);

  return potential;
}
****/

  //potarray = GetPotential("AO_A:2:ASP-ASP.xst-ASPP_6.txt.txt",&p);
  //potarray = MergePotential(&para,&p);
  //printf("BAM %lf",*(potarray)); 




  /* Working Code for making into arrrays

    double *potarray[6];
  char potname[100];
  strcpy(potname,"AO_A:2:ASP-ASP.xst-ASPP_6.txt") ;
  int i = 0;
  for (i=0;i<=5;i++)
  {
  sprintf(potname,"AO_A:2:ASP-ASP.xst-ASPP_%d.txt",(i+1));
  potarray[i] = GetPotential(potname,&p);


  /*strcpy(potname,"AO_A:2:ASP-ASP.xst-ASPP_6.txt") ;

  printf("BAM %lf\n",*(potarray[i]+18));
  }


  for (i=0;i<=55;i++)
  {
    
    for (j=5;j>=0;j--)
    {
      if (potarray[j][i] != 0.00)
      {
        
        value = *(potarray[j]+i) ;
        printf("%lf\n",value);
        break;
      }
      
    }
    potential_merged[i] = value ;

  }


    for (i=0;i<=5;i++)
  {
  sprintf(potname,"AO_A:2:ASP-ASP.xst-ASPP_%d.txt",(i+1));
  potarray[i] = GetPotential(potname,&p);
//printf("BAM %lf\n",*(potarray[i]+18));
  }
  double value =0;
  int j =0;
  double potential_merged[56] = {0}; // replace with *p.natoms


  FILE * file1;
  file1 = fopen("pot.txt", "w");
  for (i =0;i<=55;i++)
  {

    fprintf(file1,"%lf\n",potarray[1][i]);
  }
  fclose(file1);
  
  
  
  
    potarray[0] = GetPotential("AO_A:2:ASP-ASP.xst-ASPP_1.txt",&p);
  printf("%lf",*potarray[0]);
  
  for (i=0;i<=5;i++)
  {
    sprintf(potname,"AO_A:2:ASP-ASP.xst-ASPP_%d.txt",i+1);
    printf("%s %d\n",potname,i+1);

    potarray[i] = GetPotential(potname,&p); // GetPotential works fine; tested
  }

  //potarray[0] = GetPotential("AO_A:2:ASP-ASP.xst-ASPP_1.txt",&p);
  //potarray[1] = GetPotential("AO_A:2:ASP-ASP.xst-ASPP_2.txt",&p);
  //potarray[2] = GetPotential("AO_A:2:ASP-ASP.xst-ASPP_3.txt",&p);
  //potarray[3] = GetPotential("AO_A:2:ASP-ASP.xst-ASPP_4.txt",&p);
  //potarray[4] = GetPotential("AO_A:2:ASP-ASP.xst-ASPP_5.txt",&p);
  //potarray[5] = GetPotential("AO_A:2:ASP-ASP.xst-ASPP_6.txt",&p);


  for (i=0;i<=55;i++)

  {
    printf("%lf\n",*(potarray[0]+i));
    //printf("%lf %lf %lf %lf %lf %lf\n",*(potarray[0]+i),*(potarray[1]+i),*(potarray[2]+i),*(potarray[3]+i),*(potarray[4]+i),*(potarray[5]+i));

  }
  double* jagged[2] = {GetPotential("AO_A:2:ASP-ASP.xst-ASPP_1.txt",&p),GetPotential("AO_A:2:ASP-ASP.xst-ASPP_2.txt",&p)};
   
  //Array to hold size of each row
  int Size[2] = {56,56}, k=0;

  // Print the elements

  for (int i = 0; i<2 ; i++) // Number of Rows
  {

    //pointer to hold row address
    double *ptr = jagged[i];

    for (int j=0;j < Size[k];j++)
    {
      printf("%lf ",*ptr);
      //Move the pointer to next element of same row
      ptr++;
    }
    printf("\n");
    k++;
    //Moving the pointer to next row
    jagged[i]++;

  }


 /////////////NEWCODE///////////////////////////////////////////////

       /*for (i=0;i<=5;i++) // Replace 5 with *para.nfocus
      {
        sprintf(potname,"AO_%s-%s_%d.txt",(*sites).name[i], (*s).statelabel[k],i+1);
        GetPotential(potname,&p,arr[i]);
      }*/

        /*for (i=0;i<=55;i++)
        {
          double value = 0.0;
          for (j=5;j>=0;--j)
          {
            if (arr[j][i] != 0.00)
            {
             value = arr[j][i];
              break;
            }     
          }
          potmerge[i] = value;
        }

        /*for (j=1; j<=(*s). nsiteatoms; j++)
        {
          atnum = (*s).siteatoms[j];
          pvalue = (potmerge[atnum])*((*s).sitecharges[j][k]);
          printf("Site %d State %d atnum %d sitecharge %lf potmerge %lf Energy %lf\n",i,k,atnum,(*s).sitecharges[j][k],potmerge[atnum],pvalue);
        }*/
  


  /********************************************************************

  void IteratePot(Sites *sites, Mol *p,PrepTitraPara *para)

{
  Site *s;
  char potname[100];
  char potoutput[100];
  char potmodel[100];
  //double potofmodel[100] = {0.00};
  // Replace with [*para.nfocus][*p.natoms]
  double arr[6][56];
  double potmerge[56]; 
  double potofmodel[100]; //Number of site atoms in each state
  int atnum = 0;
  //double potvalue[2][2] = 0.0;
  double pvalue =0.0;

  FILE *fileb;
  fileb = fopen("Born","w");
  double totalenergy=0.0;
  
  int i=0,j=0,k=0,l=0,m=0,line=0,numsatms = 0;
  double systemenergy =0.0 ;
  for (i=1; i<=(*sites).nsites; i++)
  {
    s = &(*sites).site[i];
    for (k=1; k<=(*s).nstates; k++)
    {
      double arr[6][56]  = { 0 }; // replace 6 with *para.nfocus and 56 with *p.natoms
      for (j=0;j<=5;j++) // Replace 5 with *para.nfocus -1
      {
      sprintf(potname,"AO_%s-%s_%d.txt",(*sites).name[i], (*s).statelabel[k],j+1);
      printf("%s\n",potname);
      printf("Site(i)=%d State(k)=%d Potentialstep (j)=%d\n",i,k,j+1);
      
      GetPotential(potname,&p,arr[j]);
      }
      
      // MODEL POTENTIAL
      sprintf(potmodel,"AO_%s-%s_MODEL.txt",(*sites).name[i], (*s).statelabel[k]);
      printf("%s\n",potmodel);
      GetPotential(potmodel,&p,potofmodel);
      
      double potmerge[56] = {0.00};
      for (l=0;l<=55;l++) //56 ATOMS
        {
          double value = 0.0;
          for (j=5;j>=0;--j) //6 FOCUS STEPS
          {
            if (arr[j][l] != 0.00)
            {
             value = arr[j][l];
              break;
            }     
          }
          potmerge[l] = value;
          //printf("%lf and %d\n",potmerge[l],l);
        }

        double energystate = 0.0;
        numsatms = (*s).nsiteatoms ;
        for (m=1;m<=numsatms;m++) // Indexing of siteatoms starts from 1 and use <=
        { 
          line = (*s).siteatoms[m];
          energystate += potmerge[line] - potofmodel[m-1] * (*s).sitecharges[m][k];
          
        }
        printf("%lf Site %d State %s \n",energystate,i,(*s).statelabel[k]);
        totalenergy += energystate;
        fprintf(fileb,"Energy =  %lf Site %s State %s \n",energystate,(*sites).name[i],(*s).statelabel[k]);
        

         //Getting potential from Model 

    } //END OF STATE LOOP

  } // END OF SITE LOOP

fprintf(fileb,"Total Born Energy in multiples of kT/e = %lf\n",totalenergy);
}




************************************************************/